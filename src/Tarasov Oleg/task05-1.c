#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <locale.h>
#include <stdlib.h>
#include <string.h>

#define MAX_STRING 100
#define FILE_PATH "C:\\Temp\\Books.txt"

struct BOOK {
	char title[32];
	char author[32];
	int year;
};

int ReadBookInfo(char* string, char* title, char* author, int* year)
{
	int counter = 0;
	char *pstr, temp_str[MAX_STRING];
	strcpy(temp_str, string); 

	/* ������ �������� ����� */
	pstr = strtok(temp_str, "|");
	if (pstr == NULL)
		return counter;
	strcpy(title, pstr);
	counter++;

	/* ������ ������ ����� */
	pstr = strtok(NULL, "|");
	if (pstr == NULL)
		return counter;
	strcpy(author, pstr);
	counter++;

	/* ������ ���� ������� */
	pstr = strtok(NULL, "|");
	*year = atoi(pstr);
	if (*year == 0)
		return counter;
	counter++;
	return counter;
}

void PrintBookInfo(int i, const char* title, const char* author, int year)
{
	printf("%d. %s. '%s' (%d)\n", i, author, title, year);
}

void BooksSort(int* AlphOrder, int num, const BOOK* books)
{
	for (int i = 1; i < num; i++) {
		int j = i - 1, key = AlphOrder[i];
		while (j >= 0 && strcmp(books[AlphOrder[j]].author, books[key].author) > 0) {
			AlphOrder[j + 1] = AlphOrder[j];
			j--;
		}
		AlphOrder[j + 1] = key;
	}
}

int main()
{
	setlocale(LC_ALL, "Russian");
	FILE *fp = fopen(FILE_PATH, "r");
	char string[MAX_STRING], temp_title[32], temp_author[32];
	int temp_year = 0, num = 0, most_old = 0, most_new = 0;
	struct BOOK * books = (struct BOOK*)calloc(num, sizeof(BOOK));

	if (fp == NULL) {
		printf("���� �� ������!\n");
		return 1;
	}

	while (fgets(string, MAX_STRING, fp)) {
		if (string[0] == 0 || string[0] == '\n') break;
		if (ReadBookInfo(string, temp_title, temp_author, &temp_year) == 3) {
			books = (struct BOOK*)realloc(books, ++num * sizeof(struct BOOK));
			strcpy(books[num - 1].title, temp_title);
			strcpy(books[num - 1].author, temp_author);
			books[num - 1].year = temp_year;
			if (books[num - 1].year < books[most_old].year)
				most_old = num - 1;
			else if (books[num - 1].year > books[most_new].year)
				most_new = num - 1;
		}
		else {
			printf("������ ����������!\n");
			break;
		}
	}

	printf("*** ���������� � ������ ***\n");
	for (int i = 0; i < num; i++) 
		PrintBookInfo(i + 1, books[i].title, books[i].author, books[i].year);

	printf("\n* ����� ������ �����:\n");
	PrintBookInfo(1, books[most_old].title, books[most_old].author, books[most_old].year);
	printf("\n* ����� ����� �����:\n");
	PrintBookInfo(2, books[most_new].title, books[most_new].author, books[most_new].year);

	int* AlphOrder = (int*)calloc(num, sizeof(int));
	for (int i = 0; i < num; i++)
		AlphOrder[i] = i;

	BooksSort(AlphOrder, num, books);

	printf("\n* ����� � ���������� ������� (�� �������� �������):\n");
	for (int i = 0; i < num; i++) 
		PrintBookInfo(i + 1, books[AlphOrder[i]].title, books[AlphOrder[i]].author, books[AlphOrder[i]].year);

	free(AlphOrder);
	free(books);
	return 0;
}