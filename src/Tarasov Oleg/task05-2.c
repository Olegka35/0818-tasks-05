#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <locale.h>
#include <stdlib.h>

#define FILE_PATH "C://Temp//text.txt"

struct SymbolCount {
	char sym;
	int count;
};

void ReadString(char* string, int* num, int* pmarks, int* digits, int* words, int* wordslength, char* most_popular)
{
	int i = 0, k = 0, difwords = 0, max = 0;
	struct SymbolCount* scp = (struct SymbolCount*)calloc(difwords, sizeof(struct SymbolCount));
	while (string[i] != 0 && string[i] != '\n') {
		(*num)++;
		if (string[i] != ' ') {
			(*words)++;
			k = i;
			while (string[i] != ' ' && string[i] != '\n' && string[i] != 0) {
				int newsym = 1;
				for (int l = 0; l < difwords; l++) {
					if (scp[l].sym == string[i]) {
						newsym = 0;
						scp[l].count++;
					}
				}
				if (newsym == 1) {
					scp = (struct SymbolCount*)realloc(scp, (++difwords)*sizeof(struct SymbolCount));
					scp[difwords - 1].sym = string[i];
					scp[difwords - 1].count = 1;
				}

				if (i != k)
					(*num)++; // ������ string[k] ��� �������� � ������ �������� �����
				if (string[i] == ',' || string[i] == '.' || string[i] == ':' || string[i] == '-' || string[i] == ';')
				{
					(*pmarks)++;
				}
				if (string[i] > '0' && string[i] <= '9')
				{
					printf("%d\n", string[i]);
					(*digits)++;
				}
				(*wordslength)++;
				i++;
			}
			i--;
		}
		i++;
	}
	for (int i; i < sizeof(scp); i++) {
		if (scp[i].count > max) {
			max = scp[i].count;
			*most_popular = scp[i].sym;
		}
	}

	free(scp);
	scp = NULL;
}

int main()
{
	setlocale(LC_ALL, "Russian");
	FILE* fp = fopen(FILE_PATH, "r");
	char string[100];
	int num = 0, pmarks = 0, digits = 0, words = 0, wordslength = 0;
	char most_popular = ' ';

	if (fp == NULL) {
		printf("�� ������� ������� ����!");
		return 1;
	}

	while (fgets(string, 100, fp)) {
		ReadString(string, &num, &pmarks, &digits, &words, &wordslength, &most_popular);
	}
	printf("%d %d", '�', '!');
	printf("���������� � �����:\n\
		   1) ���������� ��������: %d\n\
 		   2) ���������� ����: %d\n\
   		   3) ���������� ������ ����������: %d\n\
   		   4) ���������� ����: %d\n\
   		   5) ������� ����� �����: %.2f ����.\n\
  		   6) ����� ���������� ������: %c\n", num, words, pmarks, digits, float(wordslength) / float(words), most_popular);
	return 0;
}